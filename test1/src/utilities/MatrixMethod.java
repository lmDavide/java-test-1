package utilities;
public class MatrixMethod {
	public int[][] matrixArray;
	
	//duplicate method to duplicate the array
	public int[][] duplicate(int[][] inputArray){
		int newArrayRow = inputArray.length;
		int newArrayColumn = (inputArray[0].length * 2 );
		this.matrixArray = new int[newArrayRow][newArrayColumn];
		
		for (int countRow = 0; countRow < this.matrixArray.length; countRow++ ) {
			int inputArrayCountCol = 0;
			for (int countCol = 0; countCol < this.matrixArray[0].length; countCol++ ) {
				this.matrixArray[countRow][countCol] = inputArray[countRow][inputArrayCountCol];
				countCol++;
				this.matrixArray[countRow][countCol] = inputArray[countRow][inputArrayCountCol];
				inputArrayCountCol++;	
			}
		}
		
		return this.matrixArray;
	}
}