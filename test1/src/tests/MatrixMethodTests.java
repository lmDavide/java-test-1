package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import utilities.MatrixMethod;

class MatrixMethodTests {

	@Test
	void testOne() {
		int[][] newArray = new int[4][5];
		int number = 0;
		for (int rowCount = 0; rowCount < newArray.length; rowCount++) {
			for (int colCount = 0; colCount < newArray[0].length; colCount++) {
				newArray[rowCount][colCount] = number;
				number++;
			}
		}
		
		MatrixMethod matrix = new MatrixMethod();
		matrix.duplicate(newArray);
		
		assertEquals(matrix.matrixArray[0][0] , 0);
	}
	
	@Test
	void testTwo() {
		int[][] newArray = new int[9][3];
		int number = 0;
		for (int rowCount = 0; rowCount < newArray.length; rowCount++) {
			for (int colCount = 0; colCount < newArray[0].length; colCount++) {
				newArray[rowCount][colCount] = number;
				number++;
			}
		}
		
		MatrixMethod matrix = new MatrixMethod();
		matrix.duplicate(newArray);
		
		assertEquals(matrix.matrixArray[8][2] , 25);
	}
	
	@Test
	void testThree() {
		int[][] newArray = new int[9][3];
		int number = 0;
		for (int rowCount = 0; rowCount < newArray.length; rowCount++) {
			for (int colCount = 0; colCount < newArray[0].length; colCount++) {
				newArray[rowCount][colCount] = number;
				number++;
			}
		}
		
		MatrixMethod matrix = new MatrixMethod();
		matrix.duplicate(newArray);
		
		assertEquals(matrix.matrixArray[8][5] , 26);
	}

}
