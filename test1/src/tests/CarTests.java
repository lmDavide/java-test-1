// David Tran 1938381

package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import automobiles.Car;

class CarTests {

	@Test
	void testGetSpeed() {
		Car newCar = new Car(10);
		assertEquals( newCar.getSpeed() , 10 );
	}
	
	@Test
	void testGetLocation() {
		Car newCar = new Car(10);
		assertEquals( newCar.getLocation() , 50); // (MAX_POSITION = 100, so 100/2 = 50
	}
	
	@Test
	void testMoveLeftOne() {
		Car newCar = new Car(44);
		assertEquals( newCar.getSpeed() , 44 );
		assertEquals( newCar.getLocation() , 50);
		newCar.moveLeft();
		assertEquals( newCar.getLocation() , 6);
	}
	
	@Test
	void testMoveLeftTwo() {
		Car newCar = new Car(79);
		assertEquals( newCar.getSpeed() , 79 );
		assertEquals( newCar.getLocation() , 50);
		newCar.moveLeft();
		assertEquals( newCar.getLocation() , 0);
	}
	
	@Test
	void testMoveRightOne() {
		Car newCar = new Car(35);
		assertEquals( newCar.getSpeed() , 35 );
		assertEquals( newCar.getLocation() , 50);
		newCar.moveRight();
		assertEquals( newCar.getLocation() , 85 );
	}
	
	@Test
	void testMoveRightTwo() {
		Car newCar = new Car(51);
		assertEquals( newCar.getSpeed() , 51 );
		assertEquals( newCar.getLocation() , 50);
		newCar.moveRight();
		assertEquals( newCar.getLocation() , 100 );
	}
	
	@Test
	void testAccelerate() {
		Car newCar = new Car(10);
		assertEquals( newCar.getSpeed() , 10 );
		newCar.accelerate();
		assertEquals( newCar.getSpeed() , 11 );
	}
	
	@Test
	void testStop() {
		Car newCar = new Car(10);
		assertEquals( newCar.getSpeed() , 10 );
		newCar.stop();
		assertEquals( newCar.getSpeed() , 0 );
	}

}
